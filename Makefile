NAME = planetfilter
VERSION := $(shell grep "VERSION =" $(NAME) | perl -pe 's/^.+?([0-9.]+).$$/$$1/g')

all:

clean:
	find -name "*.pyc" -delete

pyflakes:
	@echo Running pyflakes...
	@pyflakes3 $(NAME)

pydocstyle:
	@echo Running pydocstyle...
	@pydocstyle $(NAME)

pep8:
	@echo Running pep8...
	@pep8 $(NAME)

codespell:
	@echo Running codespell...
	@codespell $(NAME)

lint:
	@echo Running pylint...
	@pylint3 --rcfile=.pylintrc $(NAME)

test: pep8 pydocstyle pyflakes lint codespell

dist: test
	git commit -a -m "Bump version and changelog for release"
	git tag -s $(NAME)-$(VERSION) -m "$(VERSION) release"
	python setup.py sdist

upload:
	python setup.py sdist upload --sign
